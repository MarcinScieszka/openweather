# Slovakian translation for gnome-shell-extension-openweather
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Dušan Kazik <prescott66@gmail.com>, 2011,2016, 2017.
# Jose Riha <jose1711 gmail com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-17 16:52-0300\n"
"PO-Revision-Date: 2022-05-04 22:10+0200\n"
"Last-Translator: Jose Riha <jose1711@gmail.com>\n"
"Language-Team: \n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Poedit 3.0.1\n"

#: src/extension.js:267
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Služba openweathermap.org nefunguje bez kľúča api-key.\n"
"Buď zapnite prepínač v nastaveniach na použitie predvoleného kľúča "
"rozšírenia, alebo sa zaregistrujte na adrese https://openweathermap.org/"
"appid a vložte váš súkromný kľúč do dialógového okna nastavení."

#: src/extension.js:454 src/extension.js:466
#, javascript-format
msgid "Can not connect to %s"
msgstr "Nedá sa pripojiť k %s"

#: src/extension.js:769 src/preferences/locationsPage.js:39
#: src/preferences/locationsPage.js:58
msgid "Locations"
msgstr "Umiestnenia"

#: src/extension.js:770
msgid "Reload Weather Information"
msgstr "Znovu načítať informácie o počasí"

#: src/extension.js:771
#, fuzzy, javascript-format
msgid "Weather data by: %s"
msgstr "Údaje o počasí získané z:"

#: src/extension.js:773
msgid "Weather Settings"
msgstr "Nastavenia počasia"

#: src/extension.js:787
msgid "Manual refreshes less than 2 minutes apart are ignored!"
msgstr ""

#: src/extension.js:801
#, javascript-format
msgid "Can not open %s"
msgstr "Nedá sa otvoriť %s"

#: src/extension.js:849 src/preferences/locationsPage.js:509
msgid "Invalid city"
msgstr "Neplatné mesto"

#: src/extension.js:860
msgid "Invalid location! Please try to recreate it."
msgstr "Neplatné umiestnenie! Prosím, skúste ho vytvoriť znovu."

#: src/extension.js:906 src/preferences/generalPage.js:148
msgid "°F"
msgstr "°F"

#: src/extension.js:908 src/preferences/generalPage.js:149
msgid "K"
msgstr "K"

#: src/extension.js:910 src/preferences/generalPage.js:150
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:912 src/preferences/generalPage.js:151
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:914 src/preferences/generalPage.js:152
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:916 src/preferences/generalPage.js:153
msgid "°De"
msgstr "°De"

#: src/extension.js:918 src/preferences/generalPage.js:154
msgid "°N"
msgstr "°N"

#: src/extension.js:920 src/preferences/generalPage.js:147
msgid "°C"
msgstr "°C"

#: src/extension.js:957
msgid "Calm"
msgstr "Bezvetrie"

#: src/extension.js:960
msgid "Light air"
msgstr "Vánok"

#: src/extension.js:963
msgid "Light breeze"
msgstr "Vetrík"

#: src/extension.js:966
msgid "Gentle breeze"
msgstr "Slabý vietor"

#: src/extension.js:969
msgid "Moderate breeze"
msgstr "Mierny vietor"

#: src/extension.js:972
msgid "Fresh breeze"
msgstr "Čerstvý vietor"

#: src/extension.js:975
msgid "Strong breeze"
msgstr "Silný vietor"

#: src/extension.js:978
msgid "Moderate gale"
msgstr "Mierna víchrica"

#: src/extension.js:981
msgid "Fresh gale"
msgstr "Čerstvá víchrica"

#: src/extension.js:984
msgid "Strong gale"
msgstr "Silná víchrica"

#: src/extension.js:987
msgid "Storm"
msgstr "Búrka"

#: src/extension.js:990
msgid "Violent storm"
msgstr "Silná búrka"

#: src/extension.js:993
msgid "Hurricane"
msgstr "Hurikán"

#: src/extension.js:997
msgid "Sunday"
msgstr "Nedeľa"

#: src/extension.js:997
msgid "Monday"
msgstr "Pondelok"

#: src/extension.js:997
msgid "Tuesday"
msgstr "Utorok"

#: src/extension.js:997
msgid "Wednesday"
msgstr "Streda"

#: src/extension.js:997
msgid "Thursday"
msgstr "Štvrtok"

#: src/extension.js:997
msgid "Friday"
msgstr "Piatok"

#: src/extension.js:997
msgid "Saturday"
msgstr "Sobota"

#: src/extension.js:1003
msgid "N"
msgstr "S"

#: src/extension.js:1003
msgid "NE"
msgstr "SV"

#: src/extension.js:1003
msgid "E"
msgstr "Z"

#: src/extension.js:1003
msgid "SE"
msgstr "JV"

#: src/extension.js:1003
msgid "S"
msgstr "J"

#: src/extension.js:1003
msgid "SW"
msgstr "JZ"

#: src/extension.js:1003
msgid "W"
msgstr "Z"

#: src/extension.js:1003
msgid "NW"
msgstr "SZ"

#: src/extension.js:1056 src/extension.js:1065
#: src/preferences/generalPage.js:177
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1060 src/preferences/generalPage.js:178
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1070 src/preferences/generalPage.js:179
msgid "bar"
msgstr "bar"

#: src/extension.js:1075 src/preferences/generalPage.js:180
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1080 src/preferences/generalPage.js:181
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1085 src/preferences/generalPage.js:182
msgid "atm"
msgstr "atm"

#: src/extension.js:1090 src/preferences/generalPage.js:183
msgid "at"
msgstr "at"

#: src/extension.js:1095 src/preferences/generalPage.js:184
msgid "Torr"
msgstr "Torr"

#: src/extension.js:1100 src/preferences/generalPage.js:185
msgid "psi"
msgstr "psi"

#: src/extension.js:1105 src/preferences/generalPage.js:186
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1110 src/preferences/generalPage.js:187
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1158 src/preferences/generalPage.js:165
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1163 src/preferences/generalPage.js:164
msgid "mph"
msgstr "mph"

#: src/extension.js:1168 src/preferences/generalPage.js:163
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1177 src/preferences/generalPage.js:166
msgid "kn"
msgstr "kn"

#: src/extension.js:1182 src/preferences/generalPage.js:167
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1227 src/extension.js:1265
msgid "Loading ..."
msgstr "Načítava sa…"

#: src/extension.js:1269
msgid "Please wait"
msgstr "Prosím, čakajte"

#: src/extension.js:1340
msgid "Feels Like:"
msgstr "Pocitová teplota:"

#: src/extension.js:1344
msgid "Humidity:"
msgstr "Vlhkosť:"

#: src/extension.js:1348
msgid "Pressure:"
msgstr "Tlak:"

#: src/extension.js:1352
msgid "Wind:"
msgstr "Vietor:"

#: src/extension.js:1356
msgid "Gusts:"
msgstr "V nárazoch:"

#: src/extension.js:1487
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1489
#, fuzzy, javascript-format
msgid "%s Day Forecast"
msgstr "Stav počasia v predpovedi"

#: src/openweathermap.js:49
#, fuzzy
msgid "Thunderstorm with Light Rain"
msgstr "búrka so slabým dažďom"

#: src/openweathermap.js:51
#, fuzzy
msgid "Thunderstorm with Rain"
msgstr "búrka s dažďom"

#: src/openweathermap.js:53
#, fuzzy
msgid "Thunderstorm with Heavy Rain"
msgstr "búrka so silným dažďom"

#: src/openweathermap.js:55
#, fuzzy
msgid "Light Thunderstorm"
msgstr "slabá búrka"

#: src/openweathermap.js:57
msgid "Thunderstorm"
msgstr "búrka"

#: src/openweathermap.js:59
#, fuzzy
msgid "Heavy Thunderstorm"
msgstr "silná búrka"

#: src/openweathermap.js:61
#, fuzzy
msgid "Ragged Thunderstorm"
msgstr "ojedinelé búrky"

#: src/openweathermap.js:63
#, fuzzy
msgid "Thunderstorm with Light Drizzle"
msgstr "búrka so slabým mrholením"

#: src/openweathermap.js:65
#, fuzzy
msgid "Thunderstorm with Drizzle"
msgstr "búrka s mrholením"

#: src/openweathermap.js:67
#, fuzzy
msgid "Thunderstorm with Heavy Drizzle"
msgstr "búrka so silným mrholením"

#: src/openweathermap.js:69
#, fuzzy
msgid "Light Drizzle"
msgstr "Vetrík"

#: src/openweathermap.js:71
msgid "Drizzle"
msgstr "mrholenie"

#: src/openweathermap.js:73
#, fuzzy
msgid "Heavy Drizzle"
msgstr "mrholenie"

#: src/openweathermap.js:75
#, fuzzy
msgid "Light Drizzle Rain"
msgstr "dážď s mrholením"

#: src/openweathermap.js:77
#, fuzzy
msgid "Drizzle Rain"
msgstr "dážď s mrholením"

#: src/openweathermap.js:79
#, fuzzy
msgid "Heavy Drizzle Rain"
msgstr "dážď s mrholením"

#: src/openweathermap.js:81
#, fuzzy
msgid "Shower Rain and Drizzle"
msgstr "prehánky s mrholením"

#: src/openweathermap.js:83
#, fuzzy
msgid "Heavy Rain and Drizzle"
msgstr "silné prehánky a mrholenie"

#: src/openweathermap.js:85
#, fuzzy
msgid "Shower Drizzle"
msgstr "prehánky s mrholením"

#: src/openweathermap.js:87
#, fuzzy
msgid "Light Rain"
msgstr "slabý dážď"

#: src/openweathermap.js:89
#, fuzzy
msgid "Moderate Rain"
msgstr "mierny dážď"

#: src/openweathermap.js:91
#, fuzzy
msgid "Heavy Rain"
msgstr "silné sneženie"

#: src/openweathermap.js:93
#, fuzzy
msgid "Very Heavy Rain"
msgstr "veľmi silný dážď"

#: src/openweathermap.js:95
#, fuzzy
msgid "Extreme Rain"
msgstr "extrémny dážď"

#: src/openweathermap.js:97
#, fuzzy
msgid "Freezing Rain"
msgstr "mrznúci dážď"

#: src/openweathermap.js:99
#, fuzzy
msgid "Light Shower Rain"
msgstr "slabé snehové prehánky"

#: src/openweathermap.js:101
#, fuzzy
msgid "Shower Rain"
msgstr "prehánky"

#: src/openweathermap.js:103
#, fuzzy
msgid "Heavy Shower Rain"
msgstr "silné snehové prehánky"

#: src/openweathermap.js:105
#, fuzzy
msgid "Ragged Shower Rain"
msgstr "ojedinelé prehánky"

#: src/openweathermap.js:107
#, fuzzy
msgid "Light Snow"
msgstr "slabé sneženie"

#: src/openweathermap.js:109
msgid "Snow"
msgstr "sneženie"

#: src/openweathermap.js:111
#, fuzzy
msgid "Heavy Snow"
msgstr "silné sneženie"

#: src/openweathermap.js:113
msgid "Sleet"
msgstr "poľadovica"

#: src/openweathermap.js:115
#, fuzzy
msgid "Light Shower Sleet"
msgstr "snehové prehánky s dažďom"

#: src/openweathermap.js:117
#, fuzzy
msgid "Shower Sleet"
msgstr "snehové prehánky s dažďom"

#: src/openweathermap.js:119
#, fuzzy
msgid "Light Rain and Snow"
msgstr "slabý dážď so snehom"

#: src/openweathermap.js:121
#, fuzzy
msgid "Rain and Snow"
msgstr "dážď so snehom"

#: src/openweathermap.js:123
#, fuzzy
msgid "Light Shower Snow"
msgstr "slabé snehové prehánky"

#: src/openweathermap.js:125
#, fuzzy
msgid "Shower Snow"
msgstr "snehové prehánky"

#: src/openweathermap.js:127
#, fuzzy
msgid "Heavy Shower Snow"
msgstr "silné snehové prehánky"

#: src/openweathermap.js:129
msgid "Mist"
msgstr "hmla"

#: src/openweathermap.js:131
msgid "Smoke"
msgstr "dym"

#: src/openweathermap.js:133
msgid "Haze"
msgstr "opar"

#: src/openweathermap.js:135
msgid "Sand/Dust Whirls"
msgstr "Piesočné/prašné víry"

#: src/openweathermap.js:137
msgid "Fog"
msgstr "Hmla"

#: src/openweathermap.js:139
msgid "Sand"
msgstr "piesok"

#: src/openweathermap.js:141
msgid "Dust"
msgstr "prach"

#: src/openweathermap.js:143
msgid "Volcanic Ash"
msgstr ""

#: src/openweathermap.js:145
msgid "Squalls"
msgstr ""

#: src/openweathermap.js:147
msgid "Tornado"
msgstr ""

#: src/openweathermap.js:149
#, fuzzy
msgid "Clear Sky"
msgstr "Vymaže položku"

#: src/openweathermap.js:151
#, fuzzy
msgid "Few Clouds"
msgstr "zopár oblakov"

#: src/openweathermap.js:153
#, fuzzy
msgid "Scattered Clouds"
msgstr "ojedinelá oblačnosť"

#: src/openweathermap.js:155
#, fuzzy
msgid "Broken Clouds"
msgstr "potrhaná oblačnosť"

#: src/openweathermap.js:157
#, fuzzy
msgid "Overcast Clouds"
msgstr "zamračené"

#: src/openweathermap.js:159
msgid "Not available"
msgstr "Nedostupný"

#: src/openweathermap.js:373 src/openweathermap.js:375
msgid ", "
msgstr ", "

#: src/openweathermap.js:391
msgid "?"
msgstr "?"

#: src/openweathermap.js:459
msgid "Tomorrow"
msgstr "Zajtra"

#: src/preferences/generalPage.js:31
#, fuzzy
msgid "Settings"
msgstr "Nastavenia počasia"

#: src/preferences/generalPage.js:39
msgid "General"
msgstr ""

#: src/preferences/generalPage.js:57
msgid "Current Weather Refresh"
msgstr ""

#: src/preferences/generalPage.js:58
msgid "Current weather refresh interval in minutes"
msgstr ""

#: src/preferences/generalPage.js:80
#, fuzzy
msgid "Weather Forecast Refresh"
msgstr "Predpoveď počasia v strede"

#: src/preferences/generalPage.js:81
msgid "Forecast refresh interval in minutes if enabled"
msgstr ""

#: src/preferences/generalPage.js:92
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/preferences/generalPage.js:93
msgid "Disables all fetching and processing of forecast data"
msgstr ""

#: src/preferences/generalPage.js:104
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "System Icons"
msgstr "Systémové ikony"

#: src/preferences/generalPage.js:105
msgid "Disable to use packaged Adwaita weather icons"
msgstr ""

#: src/preferences/generalPage.js:106
msgid ""
"If you have issues with your system icons displaying correctly disable this "
"to fix it"
msgstr ""

#: src/preferences/generalPage.js:126
msgid "First Boot Delay"
msgstr ""

#: src/preferences/generalPage.js:127
msgid "Seconds to delay popup initialization and data fetching"
msgstr ""

#: src/preferences/generalPage.js:128
msgid ""
"This setting only applies to the first time the extension is loaded. (first "
"log in / restarting gnome shell)"
msgstr ""

#: src/preferences/generalPage.js:142
msgid "Units"
msgstr "Jednotky"

#: src/preferences/generalPage.js:156
#, fuzzy
msgid "Temperature"
msgstr "Jednotka teploty"

#: src/preferences/generalPage.js:168
msgid "Beaufort"
msgstr "Beaufort"

#: src/preferences/generalPage.js:170
#, fuzzy
msgid "Wind Speed"
msgstr "Jednotka rýchlosti vetra"

#: src/preferences/generalPage.js:189
#, fuzzy
msgid "Pressure"
msgstr "Tlak:"

#: src/preferences/generalPage.js:201 src/preferences/locationsPage.js:66
msgid "Provider"
msgstr "Poskytovateľ"

#: src/preferences/generalPage.js:210
msgid "OpenWeatherMap Multilingual Support"
msgstr ""

#: src/preferences/generalPage.js:211
msgid "Using provider translations applies to weather conditions only"
msgstr ""

#: src/preferences/generalPage.js:212
msgid ""
"Enable this to use OWM multilingual support in 46 languages if there's no "
"built-in translations for your language yet."
msgstr ""

#: src/preferences/generalPage.js:224
msgid "Use Extensions API Key"
msgstr ""

#: src/preferences/generalPage.js:225
#, fuzzy
msgid "Use the built-in API key for OpenWeatherMap"
msgstr "Použiť predvolený API kľúč rozšírenia pre openweathermap.org"

#: src/preferences/generalPage.js:226
#, fuzzy
msgid ""
"Disable this if you have your own API key from openweathermap.org and enter "
"it below."
msgstr ""
"Vypnite, ak máte svoj vlastný kľúč api-key pre službu openweathermap.org a "
"vložte ho do textového poľa nižšie."

#: src/preferences/generalPage.js:240
msgid "Personal API Key"
msgstr ""

#: src/preferences/layoutPage.js:31
msgid "Layout"
msgstr "Rozloženie"

#: src/preferences/layoutPage.js:39
msgid "Panel"
msgstr ""

#: src/preferences/layoutPage.js:44
msgid "Center"
msgstr "V strede"

#: src/preferences/layoutPage.js:45
msgid "Right"
msgstr "Vpravo"

#: src/preferences/layoutPage.js:46
msgid "Left"
msgstr "Vľavo"

#: src/preferences/layoutPage.js:48
#, fuzzy
msgid "Position In Panel"
msgstr "Pozícia na paneli"

#: src/preferences/layoutPage.js:69
#, fuzzy
msgid "Position Offset"
msgstr "Pozícia na paneli"

#: src/preferences/layoutPage.js:70
msgid "The position relative to other items in the box"
msgstr ""

#: src/preferences/layoutPage.js:78
#, fuzzy
msgid "Show the temperature in the panel"
msgstr "Teplota v paneli"

#: src/preferences/layoutPage.js:82
#, fuzzy
msgid "Temperature In Panel"
msgstr "Teplota v paneli"

#: src/preferences/layoutPage.js:90
msgid "Show the weather conditions in the panel"
msgstr ""

#: src/preferences/layoutPage.js:94
#, fuzzy
msgid "Conditions In Panel"
msgstr "Stav počasia v lište"

#: src/preferences/layoutPage.js:107
msgid "Popup"
msgstr ""

#: src/preferences/layoutPage.js:125
msgid "Popup Position"
msgstr ""

#: src/preferences/layoutPage.js:126
msgid "Alignment of the popup from left to right"
msgstr ""

#: src/preferences/layoutPage.js:137
#, fuzzy
msgid "Wind Direction Arrows"
msgstr "Smer vetra indikovať šípkami"

#: src/preferences/layoutPage.js:149
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr "Prekladať stav počasia"

#: src/preferences/layoutPage.js:157
msgid "0"
msgstr ""

#: src/preferences/layoutPage.js:158 src/preferences/layoutPage.js:229
msgid "1"
msgstr ""

#: src/preferences/layoutPage.js:159 src/preferences/layoutPage.js:230
msgid "2"
msgstr ""

#: src/preferences/layoutPage.js:160 src/preferences/layoutPage.js:231
msgid "3"
msgstr ""

#: src/preferences/layoutPage.js:162
#, fuzzy
msgid "Temperature Decimal Places"
msgstr "Teplota v paneli"

#: src/preferences/layoutPage.js:163
#, fuzzy
msgid "Maximum number of digits after the decimal point"
msgstr "Maximálny počet desatinných miest"

#: src/preferences/layoutPage.js:183
msgid "Location Text Length"
msgstr ""

#: src/preferences/layoutPage.js:184
#, fuzzy
msgid "Maximum length of the location text. A setting of '0' is unlimited"
msgstr "Limit znakov textu umiestnenia"

#: src/preferences/layoutPage.js:200
#, fuzzy
msgid "Forecast"
msgstr "Predpoveď počasia v strede"

#: src/preferences/layoutPage.js:210
#, fuzzy
msgid "Center Today's Forecast"
msgstr "Predpoveď počasia v strede"

#: src/preferences/layoutPage.js:221
#, fuzzy
msgid "Conditions In Forecast"
msgstr "Stav počasia v predpovedi"

#: src/preferences/layoutPage.js:228
msgid "Today Only"
msgstr ""

#: src/preferences/layoutPage.js:232
msgid "4"
msgstr ""

#: src/preferences/layoutPage.js:233
msgid "5"
msgstr ""

#: src/preferences/layoutPage.js:235
#, fuzzy
msgid "Total Days In Forecast"
msgstr "Stav počasia v predpovedi"

#: src/preferences/layoutPage.js:246
msgid "Keep Forecast Expanded"
msgstr ""

#: src/preferences/locationsPage.js:54
msgid "Add"
msgstr ""

#: src/preferences/locationsPage.js:73
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
msgid "Geolocation Provider"
msgstr "Poskytovateľ geolokácie"

#: src/preferences/locationsPage.js:74
msgid "Provider used for location search"
msgstr ""

#: src/preferences/locationsPage.js:87
msgid "Personal MapQuest Key"
msgstr ""

#: src/preferences/locationsPage.js:88
#, fuzzy
msgid "Personal API Key from developer.mapquest.com"
msgstr "Osobný kľúč AppKey zo služby developer.mapquest.com"

#: src/preferences/locationsPage.js:208
#, javascript-format
msgid "Location changed to: %s"
msgstr ""

#: src/preferences/locationsPage.js:223
#, fuzzy
msgid "Add New Location"
msgstr "Umiestnenie"

#: src/preferences/locationsPage.js:244
#, fuzzy
msgid "Search by Location or Coordinates"
msgstr "Hľadať podľa umiestnenia alebo súradníc"

#: src/preferences/locationsPage.js:250
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "napr. Vaiaku, Tuvalu alebo -8.5211767,179.1976747"

#: src/preferences/locationsPage.js:252 src/preferences/locationsPage.js:337
#: src/preferences/locationsPage.js:354
msgid "Clear entry"
msgstr "Vymaže položku"

#: src/preferences/locationsPage.js:261
#, fuzzy
msgid "Search"
msgstr "Hľadám…"

#: src/preferences/locationsPage.js:284
msgid "We need something to search for!"
msgstr ""

#: src/preferences/locationsPage.js:307
#, fuzzy, javascript-format
msgid "Edit %s"
msgstr "Upraviť názov"

#: src/preferences/locationsPage.js:329
#, fuzzy
msgid "Edit Name"
msgstr "Upraviť názov"

#: src/preferences/locationsPage.js:345
#, fuzzy
msgid "Edit Coordinates"
msgstr "Upraviť súradnice"

#: src/preferences/locationsPage.js:363
msgid "Save"
msgstr "Uložiť"

#: src/preferences/locationsPage.js:396
msgid "Please complete all fields"
msgstr ""

#: src/preferences/locationsPage.js:412
#, javascript-format
msgid "%s has been updated"
msgstr ""

#: src/preferences/locationsPage.js:441
#, javascript-format
msgid "Are you sure you want to delete \"%s\"?"
msgstr ""

#: src/preferences/locationsPage.js:449
msgid "Delete"
msgstr ""

#: src/preferences/locationsPage.js:453
msgid "Cancel"
msgstr "Zrušiť"

#: src/preferences/locationsPage.js:485
#, javascript-format
msgid "%s has been deleted"
msgstr ""

#: src/preferences/locationsPage.js:531
msgid "Search Results"
msgstr "Výsledky vyhľadávania"

#: src/preferences/locationsPage.js:547
msgid "New Search"
msgstr ""

#: src/preferences/locationsPage.js:554
msgid "Searching ..."
msgstr "Hľadám…"

#: src/preferences/locationsPage.js:555
#, javascript-format
msgid "Please wait while searching for locations matching \"%s\""
msgstr ""

#: src/preferences/locationsPage.js:607
msgid "AppKey Required"
msgstr ""

#: src/preferences/locationsPage.js:608
#, fuzzy, javascript-format
msgid "You need an AppKey to use MapQuest, get one at: %s"
msgstr "Aby ste mohli vyhľadávať na openmapquest, potrebujete kľúč AppKey."

#: src/preferences/locationsPage.js:705
#, javascript-format
msgid "Results for \"%s\""
msgstr ""

#: src/preferences/locationsPage.js:752
#, javascript-format
msgid "%s has been added"
msgstr ""

#: src/preferences/locationsPage.js:760
msgid "API Error"
msgstr ""

#: src/preferences/locationsPage.js:761
#, fuzzy, javascript-format
msgid "Invalid data when searching for \"%s\"."
msgstr "Neplatné údaje pri hľadaní „%s“"

#: src/preferences/locationsPage.js:764
msgid "No Matches Found"
msgstr ""

#: src/preferences/locationsPage.js:765
#, fuzzy, javascript-format
msgid "No results found when searching for \"%s\"."
msgstr "Neplatné údaje pri hľadaní „%s“"

#: src/preferences/aboutPage.js:31
msgid "About"
msgstr "O rozšírení"

#: src/preferences/aboutPage.js:58
#, fuzzy
msgid ""
"Display weather information for any location on Earth in the GNOME Shell"
msgstr ""
"<span>Zobrazuje informácie o počasí pre ktorékoľvek miesto na Zemi v GNOME "
"Shelli</span>"

#: src/preferences/aboutPage.js:72
msgid "unknown"
msgstr ""

#: src/preferences/aboutPage.js:78
#, fuzzy
msgid "OpenWeather Version"
msgstr "Nastavenia počasia"

#: src/preferences/aboutPage.js:87
#, fuzzy
msgid "Git Version"
msgstr "Verzia:"

#: src/preferences/aboutPage.js:95
#, fuzzy
msgid "GNOME Version"
msgstr "Verzia:"

#: src/preferences/aboutPage.js:102
msgid "Session Type"
msgstr ""

#: src/preferences/aboutPage.js:124
#, fuzzy, javascript-format
msgid "Maintained by: %s"
msgstr "Spravuje:"

#: src/preferences/aboutPage.js:161
#, fuzzy, javascript-format
msgid "Weather data provided by: %s"
msgstr "Údaje o počasí získané z:"

#: src/preferences/aboutPage.js:172
msgid "This program comes with ABSOLUTELY NO WARRANTY."
msgstr ""

#: src/preferences/aboutPage.js:173
msgid "See the"
msgstr ""

#: src/preferences/aboutPage.js:174
msgid "GNU General Public License, version 2 or later"
msgstr ""

#: src/preferences/aboutPage.js:174
msgid "for details."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
msgid "Weather Provider"
msgstr "Poskytovateľ počasia"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr "Jednotka teploty"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
msgid "Pressure Unit"
msgstr "Jednotka tlaku"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
msgid "Wind Speed Units"
msgstr "Jednotky rýchlosti vetra"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Vyberte jednotky použité pre zobrazenie rýchlosti vetra. Povolené hodnoty sú "
"'kph', 'mph', 'm/s', 'knots', 'ft/s' alebo 'Beaufort'."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr "Smer vetra indikovať šípkami"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Vyberte, či sa má smer vetra zobrazovať pomocou šípky alebo písmena."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr "Zobrazované mesto"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr "Aktuálne mesto"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
msgid "OpenWeatherMap Multilingual Support (weather descriptions only)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr "Teplota v paneli"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr "Stav počasia v lište"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr "Stav počasia v predpovedi"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr "Pozícia na paneli"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr "Vodorovné umiestnenie ponuky."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr "Interval aktualizácie (aktuálne počasie)"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr "Limit znakov textu umiestnenia"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
msgid "Refresh interval (forecast)"
msgstr "Interval aktualizácie (predpoveď)"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
msgid "Center forecastbox."
msgstr "Predpoveď počasia v strede."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Always keep forecast expanded"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Number of days in forecast"
msgstr "Počet dní v predpovedi počasia"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
msgid "Maximal number of digits after the decimal point"
msgstr "Maximálny počet desatinných miest"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
msgid "Your personal API key from openweathermap.org"
msgstr "Váš osobný API kľúč pre openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Použiť predvolený API kľúč rozšírenia pre openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:164
msgid "Your personal AppKey from developer.mapquest.com"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:168
msgid "Seconds to delay popup initialization and data fetch on the first load"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:172
msgid "Default width for the preferences window"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:176
msgid "Default height for the preferences window"
msgstr ""
